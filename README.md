LBJ Station brings modern, luxury living to a peaceful, creekside neighborhood in North Dallas! Enjoy access to the LBJ/Central DART Rail Station via our private pedestrian bridge that is a mere 165 steps away. We feature 1 and 2 bedroom apartment homes in a quiet creek-side park setting.

Address: 8997 Vantage Point Dr, Dallas, TX 75243, USA

Phone: 214-599-8997